# Coba SOSIAGO Influencer Marketing

SOSIAGO adalah merupakan All-in-One platform influencer marketing berbasis di Jakarta. SOSIAGO mempunyai misi untuk menyederhanakan proses dan waktu dalam mencari influencer yang tepat untuk aktifitas promosi campaign produk juga layanan. Jumlah influencer terdaftar hingga postingan ini dibuat lebih dari 19 ribu.

##Cara Daftar SOSIAGO

- Klik situs sosiago.id kemudian pilih menu signup.
- Pilih influencer
- Masukan alamat Email, Nama depan, Nama belakang – dan Password. setelah itu klik daftar.
- Tunggu hingga ada email untuk dikonfirmasi, setelah konfirmasi kamu tinggal login ke SOSIAGO.
- Jika sudah login lengkapi profil seperti nama, tanggal lahir, alamat data bank dan lain sebagainya.
- Setelah selesai klik channel untuk pilih tipe channel yang dimiliki
- Untuk blog isikan nama domain blog/web – centang (Publish dan Create) untuk tarif isikan saja jangan terlalu rendah atau ketinggian. 
- Optional bisa upload gambar jika tidak mau kosongkan saja.
- Akan muncul kode script verifikasi dan letakkan/paste ke blog Anda, kemudian jika sudah tinggal verifikasi saja.
- Setelah selesai masuk pada dashboard campaign.
- Klik Campaign yang sedang running dan join Campaign yang sesuai dengan kriteria & baca brief yang telah diberikan para advertiser.
- Selamat mengeksplor!